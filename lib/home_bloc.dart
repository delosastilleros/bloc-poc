import 'dart:math';

import 'package:gastos/food.dart';
import 'package:gastos/food_generator.dart';
import 'package:gastos/home_event.dart';
import 'package:gastos/home_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//define un bloc que recibe esos eventos y maneja esos states
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  //en el constructor le das el state inicial
  HomeBloc() : super(const HomeInitialState()) {
    on<FetchDataEvent>(_onFetchDataEvent);
    //y acá le definis todas las acciones que le vas a enviar con su handler
    //"cuando recibas un FetchDataEvent, manejalo con _onFetchDataEvent"
  }

  void _onFetchDataEvent(
    FetchDataEvent event,
    Emitter<HomeState> emitter,
  ) async {
    emitter(const HomeLoadingState());
    await Future.delayed(const Duration(seconds: 2));
    bool isSucceed = Random().nextBool();
    if (isSucceed) {
      List<Food> dummyFoods = FoodGenerator.generateDummyFoods();
      emitter(HomeSuccessFetchDataState(foods: dummyFoods));
    } else {
      emitter(const HomeErrorFetchDataState(
        errorMessage: "something went very wrong :(",
      ));
    }
  }
}
